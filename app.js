/**加载需要模块*/
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var settings = require('./settings');
var flash = require('connect-flash');
var users = require('./routes/users');

// 获取Session
var session = require('express-session');
// 获取数据库
var MongoStore = require('connect-mongo')(session);
// 生成  express 实例 app
var app = express();
// 启用保存日志
app.use(logger({stream: accessLog}));
var fs = require('fs');
var accessLog = fs.createWriteStream('access.log', {flags: 'a'});
var errorLog = fs.createWriteStream('error.log', {flags: 'a'});
var mime = require('mime');
var multer = require('multer');
var crypto = require('crypto');

app.use(session({
  secret: settings.cookieSecret,
  key: settings.db,//cookie name
  cookie: {maxAge: 1000 * 60 * 60 * 24 * 30},//30 days
  store: new MongoStore({
    db: settings.db,
    host: settings.host,
    port: settings.port
  })
}));

// 上传图片
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './resource/img');
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      // fs.renameSync('./resource/1.jpeg', './tmp/1.jpeg');
      cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
    });
  }
});

var upload = multer({ storage : storage});
app.post('/avatar', upload.single('image'), function(req, res, next) {
  next();
});

// view engine setup
// 设置 views 文件夹尾存放视图文件的目录，即
// 存放模版文件的地方, _dirname 为全局变量,存储当前正在执行的脚本所在的目录.
app.set('views', path.join(__dirname, 'views'));
// 设置视图模版引擎为 ejs
app.set('view engine', 'ejs');
app.use(flash());

// 设置/public/favicon.ico 为 favicon 图标
// 如果没有设置图标注释不要打开
app.use(favicon(__dirname + '/public/favicon.ico'));
// 加载日志中间件
app.use(logger('dev'));
// 加载解析 json 的中间件
app.use(bodyParser.json());
// 加载解析urlencoded请求体的中间件
app.use(bodyParser.urlencoded({ extended: false }));
// 加载解析cookie的中间件。
app.use(cookieParser());
// 设置public文件夹为存放静态文件的目录。
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (err, req, res, next) {
  var meta = '[' + new Date() +']' + req.url +'\n';
  errorLog.write(meta + err.stack + '\n');
  next();
});
app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
// 开发环境下的错误处理器，将错误信息渲染error模版并显示到浏览器中
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
// 生产环境下的错误处理器，将错误信息渲染error模版并显示到浏览器中
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

// 导出app实例供其他模块调用
module.exports = app;

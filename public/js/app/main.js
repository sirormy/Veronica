requirejs.config({
    baseUrl: '/js',
    paths: {
        jquery: 'jquery-1.11.3.min',
        jqueryUI: 'jquery-ui.min',
        resize: 'jquery.ba-resize',
        tagit: 'tagit',
        highlight: 'highlight.pack',
        bootstrap: 'bootstrap.min',
        validator: 'validator.min',
        marked: 'marked'
    },
    shim: {
        resize: {
            deps: ['jquery'],
            exports: '$.resize'
        },
        tagit: {
            deps: ['jquery', 'jqueryUI'],
            exports: '$.widget'
        },
        bootstrap: {
            deps: ['jquery'],
            exports: '$.fn'
        },
        validator: {
          deps: ['jquery'],
          exports: '$.fn'
        }
    }
});

require(['jquery', 'marked', 'highlight', 'tagit', 'bootstrap'], function($, marked) {

    // 初始化标签
    $('#tag').tagit({
        placeholderText: "标签:如php"
    });

    // 设置markdown参数
    marked.setOptions({
        renderer: new marked.Renderer(),
        gfm: true,
        tables: true,
        breaks: true,
        pedantic: false,
        sanitize: true,
        smartLists: true,
        smartypants: false,
        highlight: function (code) {
            return hljs.highlightAuto(code).value;
        }
    });

    // 避免回车提交
    $('input[type="text"]').on('keydown', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
        }
    });

    // 左右同步
    $('#code').on('input', function() {
      var source = $('#code').val();
      $('#wmd-preview').html( marked(source) );
      autoSave();
    });

    // 编辑页加载当前文章
    var $postName = $('#postName');
    if ($postName.length) {
      $('#postTags').val().split(',').forEach(function(el) {
        $("#tag").tagit("createTag", el);
      });
      $('#title').val($('#postName').val());
      var content = $('#postContent').val();
      $('#code').val(content);
      $('#code').trigger('input');
    }
    // 还原上次输入的内容
    else {
      var blog = localStorage.getItem('blog');
      if (blog) {
        blog = JSON.parse(blog);
        $('#title').val(blog.title);
        $('#code').val(blog.content);
        $('#code').trigger('input');
        if (blog.tags) {
          blog.tags.split(',').forEach(function(el) {
            $("#tag").tagit("createTag", el);
          });
        }

      }
    }

    // 拖动保证左右高度一致
    $('#code').bind('resize',function() {
        var souceHeight = $(this).height();
        $('#view').css('height', souceHeight+14);
    });

    // 保存
    $(document).on('keydown', function(e) {
        if(e.keyCode == 83 && (e.ctrlKey || e.metaKey)){
            e.preventDefault();
            autoSave();
        }
    });

    var timer;
    // 自动保存到LocalStorage中
    function autoSave () {
        var source = $('#code').val();
        if (source.length > 0) {
            var blog = {
                "title" : $('#title').val(),
                "tags" : $('#tag').val(),
                "post" : $('#code').val(),
                "status": 0
            };
            localStorage.setItem('blog', JSON.stringify(blog));
            var blogId = $('#postId').val();
            var url = blogId ? '/edit/'+blogId : '/post';
            /* timer && clearTimeout(timer);
            setTimeout(function() {
              $.post(url, blog, function(data) {
                var id = data.id;
                if (id) {
                  $('#postId').val(id);
                }
              }, 'json');
            }, 3000);*/
        }
    }

    // 丢弃草稿
    $('#wm-trash-button').on('click', function () {
        localStorage.removeItem('blog');
        $('#title').val('');
        $('#code').val('');
        $('#wmd-preview').empty();
        $('#tag').tagit("removeAll");
    });

    // 全屏/正常 切换
    $('#wmd-zen-button').on('click', function () {
        $('#articleText').toggleClass('editor_fullscreen');
        if ($('.editor__menu--unzen').length) {
          $('.editor__menu--unzen').attr('class', 'editor__menu--zen');
        }
        if ($('.editor__menu--zen').length) {
          $('.editor__menu--zen').attr('class', 'editor__menu--unzen');
        }
    });
    // 编辑模式，正常模式，预览模式切换
    $(document).on('click', '.editor__menu--edit:not(".muted")', function() {
        $('.muted').removeClass('muted');
        $(this).addClass('muted');
        var isFullScreen = $('#articleText').hasClass('editor_fullscreen');
        $('#articleText').attr('class', 'editor editMode');
        isFullScreen && $('#articleText').addClass('editor_fullscreen');
    });
    $(document).on('click', '.editor__menu--live:not(".muted")', function() {
        $('.muted').removeClass('muted');
        $(this).addClass('muted');
        var isFullScreen = $('#articleText').hasClass('editor_fullscreen');
        $('#articleText').attr('class', 'editor liveMode');
        if (isFullScreen) {
          $('#articleText').css('height:100%');
          $('#articleText').addClass('editor_fullscreen');
        }
    });
    $(document).on('click', '.editor__menu--preview:not(".muted")', function() {
        $('.muted').removeClass('muted');
        $(this).addClass('muted');
        var isFullScreen = $('#articleText').hasClass('editor_fullscreen');
        $('#articleText').attr('class', 'editor previewMode');
        isFullScreen && $('#articleText').addClass('editor_fullscreen');
    });

    // 插入链接
    $('#link-confirm').on('click', function () {
        var url = $.trim( $('#link-layer #link') );
    });

    // Tab 转成空格
    $(document).on('keydown', '#code', function(e) {
        if(enableTabIndent(this, e)){
          e.preventDefault();
        }

        /*var keyCode = e.keyCode || e.which;

        if (keyCode == 9) {
            e.preventDefault();
            var start = $(this).get(0).selectionStart;
            var end = $(this).get(0).selectionEnd;

            // set textarea value to: text before caret + tab + text after caret
            $(this).val($(this).val().substring(0, start)
              + "\t"
              + $(this).val().substring(end));

            // put caret at right position again
            $(this).get(0).selectionStart =
              $(this).get(0).selectionEnd = start + 1;
        }*/
    });

    // 插入链接
    $('#link-confirm').on('click', function() {
        var url = $('#link').val();
        var start = $('#code').get(0).selectionStart;
        var end = $('#code').get(0).selectionEnd;
        $('#code').val($('#code').val().substring(0,start)
            +'\r'
            +'[链接]('+url+')'
        );
        $('#link').val('');
        $('#code').trigger('input');
    });

    // 插入图片
    $('#image-confirm').on('click', function () {
      var url = 'ph-url';
      var filename = 'ph-filename';
      var start = $('#code').get(0).selectionStart;
      var end = $('#code').get(0).selectionEnd;
      $('#code').val($('#code').val().substring(0,start)
        +'\r'
        +'!['+filename+']'+'('+ url+')'
      );
    });

    // 回复
    $('.reply-btn').on('click', function () {
      var replyname = $(this).siblings('a').text();
      $('#comment').val('回复 ' + replyname + ' : ');
    });

    // 评论
    $('#commentBtn').on('click', function() {
      var content = $('#comment').val().trim();
      if (!content) return;
      $.post('/comment', {
        "id": $('article').data('id'),
        "content" : content,
        "email" : $.trim($('#email').val()),
        "website" : $.trim($('#website').val())
      }, function(data) {
        if (data.err) {
          layer.msg(data.err,{icon:2,time:1500,shade:0.5});
        } else {
          layer.msg('留言成功',{icon:1,time:1500,shade:0.5});
        }
      }, 'json');
    });

    function enableTabIndent(t, e) {
      if (e.keyCode === 9) {
        var start = t.selectionStart;
        var end = t.selectionEnd;

        var that = $(t);

        var value = that.val();
        var before = value.substring(0, start);
        var after = value.substring(end);
        var selArray = value.substring(start, end).split('\n');

        var isIndent = !e.shiftKey;

        if (isIndent) {
          if (start === end || selArray.length === 1) {
            that.val(before + '\t' + after);
            t.selectionStart = t.selectionEnd = start + 1;
          } else {
            var sel = '\t' + selArray.join('\n\t');
            that.val(before + sel + after);
            t.selectionStart = start + 1;
            t.selectionEnd = end + selArray.length;
          }
        } else {
          var reduceEnd = 0;
          var reduceStart = false;

          if (selArray.length > 1) {
            selArray.forEach(function(x, i, a) {
              if (i > 0 && x.length > 0 && x[0] === '\t') {
                a[i] = x.substring(1);
                reduceEnd++;
              }
            });
            sel = selArray.join('\n');
          } else {
            sel = selArray[0];
          }

          var b1 = '',
            b2 = '';
          if (before.length) {
            var npos = before.lastIndexOf('\n');
            if (npos !== -1) {
              b1 = before.substring(0, npos + 1);
              b2 = before.substring(npos + 1);
            } else {
              b1 = '';
              b2 = before;
            }

            sel = b2 + sel;
          }

          if (sel.length && sel[0] === '\t') {
            sel = sel.substring(1);
            reduceStart = true;
          }

          that.val(b1 + sel + after);
          t.selectionStart = start + (reduceStart ? -1 : 0);
          t.selectionEnd = end - (reduceEnd + (reduceStart ? 1 : 0));
        }
        return true;
      }
      return false;
    }

});
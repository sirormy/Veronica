const editorTheme = 'Zenburn';

// crypto 用它生成散列值来加密密码
var crypto = require('crypto'),
    User = require('../models/user.js'),
    Post = require('../models/post.js'),
    Comment = require('../models/comment.js'),
    _ = require('underscore'),
    marked = require('marked'),
    // 获取express 实例
    express = require('express');

marked.setOptions({
  renderer: new marked.Renderer(),
  gfm: true,
  tables: true,
  breaks: true,
  pedantic: false,
  sanitize: false,
  smartLists: false,
  smartypants: false
});

// 通过 express 实例创建路由实例
var router = express.Router();

// 首页
router.get('/', function (req, res) {
  //判断是否是第一页，并把请求的页数转换成 number 类型
  var page = req.query.p ? parseInt(req.query.p) : 1;
  //查询并返回第 page 页的 10 篇文章
  Post.getTen(null, page, 1, function (err, posts, total) {
    if (err) {
      posts = [];
    }
    Post.getTags(function (err, tags) {
      if (err) {
        req.flash('error', err);
        return res.redirect('/');
      }
      res.render('index', {
        title: '主页',
        curIndex : 0,
        posts: posts,
        page: page,
        tags: tags,
        marked: marked,
        isFirstPage: (page - 1) == 0,
        isLastPage: ((page - 1) * 10 + posts.length) == total,
        user: req.session.user,
        success: req.flash('success').toString(),
        error: req.flash('error').toString()
      });
    });
  });
});

// 注册页
router.get('/reg', checkNotLogin);
router.get('/reg', function (req, res) {
  res.render('reg', {
    title: '注册',
    user: req.session.user,
    curIndex: 8,
    success: req.flash('success').toString(),
    error: req.flash('error').toString()
  });
});

// 注册请求
router.post('/reg', checkNotLogin);
router.post('/reg', function (req, res) {
  var username = req.body.name,
      password = req.body.password,
      password_re = req.body['password-repeat'];
  //检验用户两次输入的密码是否一致
  if (password_re != password) {
    req.flash('error', '两次输入的密码不一致!'); 
    return res.redirect('/reg');//返回主册页
  }
  //生成密码的 md5 值
  var md5 = crypto.createHash('md5');
  password = md5.update(req.body.password).digest('hex');
  var newUser = new User({
      name: username,
      password: password,
      email: req.body.email
  });
  //检查用户名是否已经存在 
  User.get(newUser.name, function (err, user) {
    if (user) {
      req.flash('error', '用户已存在!');
      return res.redirect('/reg');//返回注册页
    }
    //如果不存在则新增用户
    newUser.save(function (err, user) {
      if (err) {
        req.flash('error', err);
        return res.redirect('/reg');//注册失败返回主册页
      }
      req.session.user = user;//用户信息存入 session
      req.flash('success', '注册成功!');
      res.redirect('/');//注册成功后返回主页
    });
  });
});

// 登录页
router.get('/login', checkNotLogin);
router.get('/login', function (req, res) {
  res.render('login', {
    title: '登录',
    curIndex: 7,
    user: req.session.user,
    success: req.flash('success').toString(),
    error: req.flash('error').toString()
  }); 
});

// 登录请求
router.post('/login', checkNotLogin);
router.post('/login', function (req, res) {
  //生成密码的 md5 值
  var md5 = crypto.createHash('md5'),
      password = md5.update(req.body.password).digest('hex');
  //检查用户是否存在
  User.get(req.body.name, function (err, user) {
    if (!user) {
      req.flash('error', '用户名或密码错误!');
      return res.redirect('/login');//用户不存在则跳转到登录页
    }
    //检查密码是否一致
    if (user.password != password) {
      req.flash('error', '用户名或密码错误!');
      return res.redirect('/login');//密码错误则跳转到登录页
    }
    //用户名密码都匹配后，将用户信息存入 session
    req.session.user = user;
    req.flash('success', '登陆成功!');
    res.redirect('/');//登陆成功后跳转到主页
  });
});

// 文章发表页
router.get('/post', checkLogin);
router.get('/post', function (req, res) {
  res.render('post', {
    title: '发表',
    user: req.session.user,
    post: {},
    editorTheme : editorTheme,
    success: req.flash('success').toString(),
    error: req.flash('error').toString()
  });
});

// 发布文章
router.post('/post', checkLogin);
router.post('/post', function (req, res) {
  var currentUser = req.session.user,
      tags = req.body.tags && _.filter(req.body.tags.split(','), function(val){return val.length > 0;}),
      post = new Post(currentUser.name, currentUser.head, req.body.title, tags, req.body.post, 1);
  var isXhr = req.xhr;
  post.save(function (err, id) {
    if (err) {
      if (isXhr) {
        res.json({err: '发表失败'});
        return;
      } else {
        req.flash('error', err);
        return res.redirect('/');
      }
    }
    if (isXhr) {
      res.json({id: id});
    } else {
      req.flash('success', '发布成功!');
      res.redirect('/');
    }
  });
});

// 登出
router.get('/logout', checkLogin);
router.get('/logout', function (req, res) {
  req.session.user = null;
  req.flash('success', '登出成功!');
  res.redirect('/');
});

// 上传文件页面
router.get('/upload', checkLogin);
router.get('/upload', function (req, res) {
  res.render('upload', {
    title: '文件上传',
    user: req.session.user,
    curIndex: 4,
    success: req.flash('success').toString(),
    error: req.flash('error').toString()
  });
});

// 上传文件
router.post('/upload', checkLogin);
router.post('/avatar', function (req, res) {
  req.flash('success', '文件上传成功!');
  res.redirect('/upload');
});

// 归档页面
router.get('/archive', function (req, res) {
  Post.getArchive(function (err, posts) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    res.render('archive', {
      title: '存档',
      posts: posts,
      curIndex: 1,
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });
});

// 文章标签列表页
router.get('/tags', function (req, res) {
  Post.getTags(function (err, posts) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    res.render('tags', {
      title: '标签',
      posts: posts,
      curIndex: 2,
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });
});

// 根据标签查询文章
router.get('/tags/:tag', function (req, res) {
  Post.getTag(req.params.tag, function (err, posts) {
    if (err) {
      req.flash('error',err); 
      return res.redirect('/');
    }
    res.render('tag', {
      title: req.params.tag,
      curIndex:0,
      posts: posts,
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });
});

// 友情链接页面
router.get('/links', function (req, res) {
  res.render('links', {
    title: '友情链接',
    curIndex: 3,
    user: req.session.user,
    success: req.flash('success').toString(),
    error: req.flash('error').toString()
  });
});

// 关键字搜索文章
router.get('/search', function (req, res) {
  Post.search(req.query.keyword, function (err, posts) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    res.render('search', {
      title: "SEARCH:" + req.query.keyword,
      posts: posts,
      curIndex: 1,
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });
});

// 用户的文章列表页面
router.get('/u/:name', function (req, res) {
  var page = req.query.p ? parseInt(req.query.p) : 1;
  //检查用户是否存在
  User.get(req.params.name, function (err, user) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    if (!user) {
      req.flash('error', '用户不存在!'); 
      return res.redirect('/');
    }
    //查询并返回该用户第 page 页的 10 篇文章
    Post.getTen(user.name, page, 1, function (err, posts, total) {
      if (err) {
        req.flash('error', err); 
        return res.redirect('/');
      }
      res.render('user', {
        title: user.name,
        posts: posts,
        page: page,
        marked: marked,
        curIndex: 1,
        isFirstPage: (page - 1) == 0,
        isLastPage: ((page - 1) * 10 + posts.length) == total,
        user: req.session.user,
        success: req.flash('success').toString(),
        error: req.flash('error').toString()
      });
    });
  }); 
});

// 文章内页
router.get('/a/:id', function (req, res) {
  Post.getOne(req.params.id, function (err, post) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('/');
    }
    res.render('article', {
      title: post.title,
      curIndex: 0,
      article: ( post.post && marked(post.post) ) || '',
      post: post,
      editorTheme : editorTheme,
      user: req.session.user,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });
});

// 发表评论
router.post('/comment', function (req, res) {
  var date = new Date(),
      time = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +
             date.getHours() + ":" + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()),
      id = req.body.id,
      content = req.body.content, head, uname;

  var user = req.session.user;
  if (user) {
    head = user.head;
    uname = user.name;
  } else {
    var email = req.body.email;
    if (email) {
      var md5 = crypto.createHash('md5'),
        email_MD5 = md5.update(req.body.email.toLowerCase()).digest('hex');
      head = "http://www.gravatar.com/avatar/" + email_MD5 + "?s=48";
    } else {
      head = "/images/user/defaultLogo.png";
    }
    uname = req.body.name || '匿名用户';
  }
  var comment = {
      name: uname,
      head: head,
      email: req.body.email,
      website: req.body.website,
      time: time,
      content: content
  };

  var newComment = new Comment(uname, time, id, comment);
  newComment.save(function (err) {
    if (err) {
      res.json('err', err);
    } else {
      res.json('success', '留言成功!');
    }
  });
});

// 编辑页面
router.get('/edit/:id', checkLogin);
router.get('/edit/:id', function (req, res) {
  var currentUser = req.session.user;
  Post.edit(currentUser.name, req.params.id, function (err, post) {
    if (err) {
      req.flash('error', err); 
      return res.redirect('back');
    }
    res.render('edit', {
      title: '编辑',
      post: post,
      user: req.session.user,
      editorTheme : editorTheme,
      success: req.flash('success').toString(),
      error: req.flash('error').toString()
    });
  });
});

// 修改文章
router.post('/edit/:id', checkLogin);
router.post('/edit/:id', function (req, res) {
  var currentUser = req.session.user,
      title = req.body.title,
      tags = req.body.tags && _.filter(req.body.tags.split(','), function(val){return val.length > 0;}),
      status = req.body.status,
      post = req.body.post;
  Post.update(currentUser.name, req.params.id, tags, title, post, status, function (err) {
    var url = '/a/'+req.params.id;
    var isXhr = req.xhr;
    if (err) {
      if (isXhr) {
        res.json({err: '修改失败!'});
      } else {
        req.flash('error', err);
        return res.redirect(url);
      }
    }
    if (isXhr) {
      res.json({suc: '修改成功!'});
    } else {
      req.flash('success', '修改成功!');
      res.redirect('/');
    }
  });
});

router.post('/remove/:id', checkLogin);
router.post('/remove/:id', function (req, res) {
  var currentUser = req.session.user;
  Post.remove(currentUser.name, req.params.id, function (err) {
    if (err) {
      res.json({tip: '删除失败'});
    } else {
      res.json({tip: '删除成功'});
    }
  });
});

router.get('/reprint/:name/:day/:title', checkLogin);
router.get('/reprint/:name/:day/:title', function (req, res) {
  Post.edit(req.params.name, req.params.day, req.params.title, 1, function (err, post) {
    if (err) {
      req.flash('error', err);
      return res.redirect('back');
    }
    var currentUser = req.session.user,
      reprint_from = {name: post.name, day: post.time.day, title: post.title},
      reprint_to = {name: currentUser.name, head: currentUser.head};
    Post.reprint(reprint_from, reprint_to, function (err, post) {
      if (err) {
        req.flash('error', err);
        return res.redirect('back');
      }
      req.flash('success', '转载成功!');
      var url = encodeURI('/u/' + post.name + '/' + post.time.day + '/' + post.title);
      res.redirect(url);
    });
  });
});

router.use(function (req, res) {
  res.render("404");
});

function checkLogin(req, res, next) {
  if (!req.session.user) {
    req.flash('error', '未登录!'); 
    res.redirect('/login');
  }
  next();
}

function checkNotLogin(req, res, next) {
  if (req.session.user) {
    req.flash('error', '已登录!'); 
    res.redirect('back');
  }
  next();
}

module.exports = router;